package test;
import java.util.Scanner;

import customers.Customer;
import products.Product;

public class App {
    public static void main(String[] args) throws Exception {
        Customer c1, c2;
        Product p1, p2;
        Scanner sc = new Scanner(System.in);
        
        
        System.out.println("\n==================Enter Customer 1 details================\n");
        c1 = new Customer();
        System.out.print("Enter customer ID __ ");
        c1.id = sc.next();
        System.out.print("Enter customer Name __ ");
        c1.name = sc.next();
        System.out.print("Enter customer City __ ");
        c1.city = sc.next();
        System.out.print("Enter customer Pincode __ ");
        c1.pincode = sc.next();
        System.out.print("Enter customer age __ ");
        c1.age = sc.nextInt();
        
        System.out.println("\n==================Enter Customer 2 details================\n");
        c2 = new Customer();
        System.out.print("Enter customer ID __ ");
        c2.id = sc.next();
        System.out.print("Enter customer Name __ ");
        c2.name = sc.next();
        System.out.print("Enter customer City __ ");
        c2.city = sc.next();
        System.out.print("Enter customer Pincode __ ");
        c2.pincode = sc.next();
        System.out.print("Enter customer age __ ");
        c2.age = sc.nextInt();

        System.out.println("\n==================Enter Product 1 details================\n");
        p1 = new Product();
        System.out.print("Enter product ID __ ");
        p1.id = sc.next();
        System.out.print("Enter product Name __ ");
        p1.name = sc.next();
        System.out.print("Enter product Country of origin __ ");
        p1.countryOfOrigin = sc.next();
        System.out.print("Enter product Category __ ");
        p1.category = sc.next();
        System.out.print("Enter product Units in stock __ ");
        p1.units = sc.nextInt();
        System.out.print("Enter product Price __ ");
        p1.price = sc.nextDouble();
        
        System.out.println("\n==================Enter Product 2 details================\n");
        p2 = new Product();
        System.out.print("Enter product ID __ ");
        p2.id = sc.next();
        System.out.print("Enter product Name __ ");
        p2.name = sc.next();
        System.out.print("Enter product Country of origin __ ");
        p2.countryOfOrigin = sc.next();
        System.out.print("Enter product Category __ ");
        p2.category = sc.next();
        System.out.print("Enter product Units in stock __ ");
        p2.units = sc.nextInt();
        System.out.print("Enter product Price __ ");
        p2.price = sc.nextDouble();

        System.out.println("=========\nCustomer 1 details are \n" +c1.getCustomerDetails());
        c1.signIn();
        System.out.println("=========\nCustomer 2 details are \n" +c2.getCustomerDetails());
        c2.signIn();

        System.out.println("=========\nProduct 1 details are \n" + p1.getProductDetails());
        System.out.println("=========\nProduct 1 category is " + p1.getProductCategory());

        System.out.println("=========\nProduct 2 details are \n" + p2.getProductDetails());
        System.out.println("=========\nProduct 2 category is " + p2.getProductCategory());

    }
}
