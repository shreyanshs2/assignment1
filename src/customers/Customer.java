package customers;
public class Customer{
    public String id, name, city, pincode;
    public int age;
    
    public String getPincode(){
        return this.pincode;
    }
    
    public String getCity(){
        return this.city;
    }

    public String getCustomerDetails(){
        return "Customer id = " + this.id + "\nCustomer name = " + this.name +  "\nCity = " + this.city;
    }

    public void signIn(){
        System.out.println(this.name + " is signed in");
    }

}