package products;
public class Product {
    public String id, name, countryOfOrigin, category;
    public int units;
    public double price;
    
    public String getProductCategory(){
        return this.category;
    }
    
    public int getUnitsAvailable(){
        return this.units;
    }

    public String getProductDetails(){
        return "Product id = " + this.id + "\nProduct name = " + this.name +  "\nPrice = " + this.price + "\nCategory = " + this.category;
    }

}
